package org.epam.rd.formatters;


import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class DecimalFormatExample {

    public static void main(String[] args) {
        String pattern = "###,###.###";
        DecimalFormat decimalFormat = new DecimalFormat(pattern);

        double number = 123456789.123;
        String formatted = decimalFormat.format(number);
        System.out.println("Formatted number " + number + " by pattern " + pattern + ": " + formatted);

        Locale ruLocale = new Locale("ru", "RU");
        DecimalFormat localizedDecimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(ruLocale);
        localizedDecimalFormat.applyPattern(pattern);
        String formattedWithLocale = localizedDecimalFormat.format(number);
        System.out.println("Formatted number " + number + " by pattern " + pattern + " with locale: " + formattedWithLocale);
    }

}
