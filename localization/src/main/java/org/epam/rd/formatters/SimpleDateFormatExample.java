package org.epam.rd.formatters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SimpleDateFormatExample {

    public static void main(String[] args) {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Date date = new Date();
        String formattedDate = simpleDateFormat.format(date);
        System.out.println("Formatted date " + date + " with pattern " + pattern + ": " + formattedDate);

        String dateToParse = "2018-01-01";
        try {
            Date parsed = simpleDateFormat.parse(dateToParse);
            System.out.println("Parsed date from " + dateToParse + ": " + parsed);
        } catch (ParseException e) {
            System.out.println("Failed to parse date: " + dateToParse);
        }
    }

}
