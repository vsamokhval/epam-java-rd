package org.epam.rd.formatters;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormatExample {

    public static void main(String[] args) {
        Locale uaLocale = new Locale("ua", "UA");
        DateFormat dateInstance = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.MEDIUM, uaLocale);
        Date date = new Date();
        String formattedDate = dateInstance.format(date);
        System.out.println("Formatted date " + date + " with locale " + uaLocale + ": " + formattedDate);
    }

}
