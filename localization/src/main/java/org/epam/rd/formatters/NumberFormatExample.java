package org.epam.rd.formatters;

import java.text.NumberFormat;
import java.util.Locale;

public class NumberFormatExample {

    public static void main(String[] args) {
        Locale uaLocale = new Locale("ua", "UA");
        NumberFormat numberFormat = NumberFormat.getInstance(uaLocale);
        double number = 100.999;
        String formatted = numberFormat.format(number);
        System.out.println("Formatted number " + number + " with locale " + uaLocale + ": " + formatted);

        NumberFormat currencyInstance = NumberFormat.getCurrencyInstance(uaLocale);
        String formattedCurrency = currencyInstance.format(number);
        System.out.println("Formatted currency " + number + " with locale " + uaLocale + ": " + formattedCurrency);
    }

}
