package org.epam.rd.i18n;

import java.math.BigDecimal;
import java.util.ListResourceBundle;


public class AppMessages extends ListResourceBundle {

    private Object[][] contents = {
        {"price", BigDecimal.valueOf(20.00)},
        {"currency", "USD"}
    };

    @Override
    protected Object[][] getContents() {
        return this.contents;
    }
}
