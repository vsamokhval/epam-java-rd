package org.epam.rd.i18n;

import java.math.BigDecimal;
import java.util.ListResourceBundle;


public class AppMessages_ru extends ListResourceBundle {

    private Object[][] contents = {
        {"price", BigDecimal.valueOf(100.00)},
        {"currency", "UAH"}
    };

    @Override
    protected Object[][] getContents() {
        return this.contents;
    }
}
