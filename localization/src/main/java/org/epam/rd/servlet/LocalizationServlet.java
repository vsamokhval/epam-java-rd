package org.epam.rd.servlet;

import org.epam.rd.util.Utf8Control;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "localizationServlet", urlPatterns = "/getLocalized")
public class LocalizationServlet extends HttpServlet {

    private ResourceBundle.Control utf8Control = new Utf8Control();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Locale locale = request.getLocale();
        ResourceBundle messages = ResourceBundle.getBundle("i18n.messages", locale, utf8Control);
        String helloMessage = messages.getString("hello_message");

        ResourceBundle prices = ResourceBundle.getBundle("org.epam.rd.i18n.AppMessages", locale);
        BigDecimal localizedPrice = (BigDecimal) prices.getObject("price");
        String localizedCurrency = prices.getString("currency");

        response.setContentType("text/html;charset=utf-8");
        PrintWriter pw = response.getWriter();
        pw.println("<h1>" + helloMessage + "</h1>");
        pw.println("<br>");
        pw.println("<h2>" + localizedPrice + " " + localizedCurrency + "</h2>");
    }
}
