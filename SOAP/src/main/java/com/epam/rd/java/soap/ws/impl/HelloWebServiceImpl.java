package com.epam.rd.java.soap.ws.impl;

import com.epam.rd.java.soap.ws.HelloWebService;

import javax.jws.WebService;

@WebService(endpointInterface = "com.epam.rd.java.soap.ws.HelloWebService")
public class HelloWebServiceImpl implements HelloWebService {
    @Override
    public String getHelloString(String name) {
        return "Hello, " + name + "!";
    }
}
