package epam.rd.concurrency;


import java.util.stream.IntStream;

public class InterruptedThread {

    public static void main(String[] args) {
        Thread heavyThread = new Thread(new EternalThread());
        heavyThread.start();
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            //Do nothing
        }
        heavyThread.interrupt();
        System.out.println("Finished after interruption");
    }

    static class EternalThread implements Runnable {

        @Override
        public void run() {
            while (true) {
                System.out.println("Starting heavy task");
                long sum = IntStream.range(0, Integer.MAX_VALUE).asLongStream().sum();
                System.out.println("Finished heavy task with result: " + sum);
                if (Thread.interrupted()) {
                    System.out.println("It's time to finish");
                    return;
                }
            }
        }
    }

}
