package epam.rd.concurrency;


public class ThreadCreation {

    public static void main(String[] args) {
        System.out.println("Running the main method in thread: " + Thread.currentThread().getName());
        Thread thread = new CustomThread();
        thread.start();
        //System.out.println("Back to main method");

        Runnable customRunnable = new CustomRunnable();
        Thread anotherThread = new Thread(customRunnable);
        anotherThread.start();

        Thread quickThread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("I was created so easy!");
            }
        });
        quickThread.start();

        Thread modernQuickThread = new Thread(() -> System.out.println("The easiest one!"));
        modernQuickThread.start();
    }


    static class CustomThread extends Thread {

        @Override
        public void run() {
            System.out.println("Thread " + this.getName() + " is running now!");
            for (int i = 0; i < 5; i++) {
                System.out.println("Do some awesome stuff " + i);
            }
            System.out.println("Good bye from thread " + this.getName());
        }
    }


    static class CustomRunnable implements Runnable {

        @Override
        public void run() {
            System.out.println("Thread " + Thread.currentThread().getName() + " is running now!");
            for (int i = 0; i < 5; i++) {
                System.out.println("Do another awesome stuff " + i);
            }
            System.out.println("Good bye from thread " + Thread.currentThread().getName());
        }
    }

}
