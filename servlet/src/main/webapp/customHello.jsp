<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/tag/hello.tld" prefix="custom" %>

<html>
    <head>
        <title>
            Custom Tag Sample
        </title>
    </head>
    <body>
        <h1>
            <custom:hello name='Peter' />
        </h1>
    </body>
</html>

